import { addNumbers } from '../src/mathUtils';

test('Add Numbers', () => {
  expect(addNumbers(1, 2)).toEqual(3);
});