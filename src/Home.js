import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Profile from './Profile'

export default class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            data: ''
        }
    }
    change(x) {
        this.setState({ data: x * 10 })
    }

    render() {
        return (
            <View style={styles.text}>
                <Profile/>
                <Text style={{ fontSize: 25 }}>Home Component</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        flex: 1, alignItems: 'center', justifyContent: 'center'
    }
})